﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;

using ImageEditorForm;

namespace FunctionTests
{
    /// <summary>
    /// Class used for testing HSLColor class - methods FromRGB(), ToRGB()
    /// </summary>
    [TestClass]
    public class HSLColorTest
    {
        [TestMethod]
        public void TestRGBtoHSL1()
        {
            HSLColor a = HSLColor.FromRGB(50,70,100);
            Assert.AreEqual(a.Hue,216f);
            Assert.AreEqual(a.Saturation,0.333f, 0.01f);
            Assert.AreEqual(a.Luminosity,0.294f, 0.01f);

        }
        [TestMethod]
        public void TestRGBtoHSL2()
        {
            HSLColor a = HSLColor.FromRGB(50, 50, 50);
            Assert.AreEqual(a.Hue, 0.0f);
            Assert.AreEqual(a.Saturation, 0.0f, 0.01f);
            Assert.AreEqual(a.Luminosity, 0.196f, 0.01f);

        }
        [TestMethod]
        public void TestRGBtoHSL3()
        {
            HSLColor a = HSLColor.FromRGB(50, 50, 100);
            Assert.AreEqual(a.Hue, 240f);
            Assert.AreEqual(a.Saturation, 0.333f, 0.01f);
            Assert.AreEqual(a.Luminosity, 0.294f, 0.01f);

        }
        [TestMethod]
        public void TestHSLtoRGB1()
        {
            HSLColor a = HSLColor.FromRGB(50, 50, 100);
            Color b = a.ToRGB();
            Assert.AreEqual(b.R, 50, 2);
            Assert.AreEqual(b.G, 50, 2);
            Assert.AreEqual(b.B,100, 2);

        }
        [TestMethod]
        public void TestHSLtoRGB2()
        {
            HSLColor a = HSLColor.FromRGB(50, 50, 50);
            Color b = a.ToRGB();
            Assert.AreEqual(b.R, 50, 2);
            Assert.AreEqual(b.G, 50, 2);
            Assert.AreEqual(b.B, 50, 2);

        }
        [TestMethod]
        public void TestHSLtoRGB3()
        {
            HSLColor a = HSLColor.FromRGB(50, 70, 100);
            Color b = a.ToRGB();
            Assert.AreEqual(b.R, 50, 2);
            Assert.AreEqual(b.G, 70,2);
            Assert.AreEqual(b.B, 100, 2);

        }
        [TestMethod]
        public void TestHSLtoRGB4()
        {
            HSLColor a = HSLColor.FromRGB(40, 98, 118);
            Color b = a.ToRGB();
            Assert.AreEqual(b.R, 40, 2);
            Assert.AreEqual(b.G, 98, 2);
            Assert.AreEqual(b.B, 118, 2);

        }
    }
}
