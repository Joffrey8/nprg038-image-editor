﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditorForm
{
    public partial class BrushOptionsForm : Form
    {
        public Color BrushColor { get; set; } = Color.Black;
        public int WidthOfPen { get; set; } = 4;

        public BrushOptionsForm()
        {
            InitializeComponent();
        }

        private void color_button_Click(object sender, EventArgs e)
        {
            ColorDialog c1 = new ColorDialog();
            c1.ShowDialog();
            BrushColor = c1.Color;
        }

        private void widthBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(widthBox.Text, out int result))
            {
                WidthOfPen = result;
            }
            else
                MessageBox.Show("Width has to be an integer!", "Wrong Input", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }
    }
}
