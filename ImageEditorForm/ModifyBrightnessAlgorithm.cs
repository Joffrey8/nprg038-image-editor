﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;


namespace ImageEditorForm
{
    class ModifyBrightnessAlgorithm
    {
        public static Bitmap ChangeBrightness(Bitmap image,bool threading, float quotient)
        {
            if (threading)
                return ChangeBrightnessThreaded(image, quotient);
            else
                return ChangeBrightnessSingleThread(image, quotient);


        }
        private static Bitmap ChangeBrightnessSingleThread(Bitmap image, float quotient)
        {
            BitmapData data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, image.PixelFormat);

            int bytesPerPixel = Bitmap.GetPixelFormatSize(image.PixelFormat) / 8;

            //The stride is the width of a single row of pixels (a scan line), rounded up to a four-byte boundary
            //Number of bytes which are needed for data container
            int NumberOfBytes = data.Stride * image.Height;
            //container where we are going to save the modified pixels
            byte[] pixels = new byte[NumberOfBytes];
            //address of the first pixel in the image
            IntPtr ptrFirstPixel = data.Scan0;
            Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
            int heightInPixels = data.Height;
            int widthInBytes = data.Width * bytesPerPixel;


            singleThread(0, heightInPixels, widthInBytes, bytesPerPixel, pixels, quotient, data.Stride);


            Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);

            image.UnlockBits(data);
            return image;
        }
        private static Bitmap ChangeBrightnessThreaded(Bitmap image, float quotient)
        {

            BitmapData data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, image.PixelFormat);

            int bytesPerPixel = Bitmap.GetPixelFormatSize(image.PixelFormat) / 8;

            //The stride is the width of a single row of pixels (a scan line), rounded up to a four-byte boundary
            //Number of bytes which are needed for data container
            int NumberOfBytes = data.Stride * image.Height;
            //container where we are going to save the modified pixels
            byte[] pixels = new byte[NumberOfBytes];
            //address of the first pixel in the image
            IntPtr ptrFirstPixel = data.Scan0;
            Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
            int heightInPixels = data.Height;
            int widthInBytes = data.Width * bytesPerPixel;

            Task[] t = new Task[Environment.ProcessorCount - 1];
            int[] starts = new int[Environment.ProcessorCount];
            starts[0] = 0; int step = heightInPixels/Environment.ProcessorCount;
            for(int i = 0; i<t.Length;i++)
            {
                int j = i;
                starts[j + 1] = starts[j] + step;
                t[j] = Task.Run(() => singleThread(starts[j], starts[j+1], widthInBytes, bytesPerPixel, pixels, quotient, data.Stride));
                
            }
            singleThread(starts[Environment.ProcessorCount-1], heightInPixels, widthInBytes, bytesPerPixel, pixels, quotient, data.Stride);
            for(int i=0;i<t.Length;i++)
            {
                while(!t[i].IsCompleted)

                    Thread.Sleep(5); //t.wait throws invalidOpEx
            }

            Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);

            image.UnlockBits(data);
            return image;
        }
        private static void singleThread(int start,int end,int widthInBytes,int bytesPerPixel,byte[] pixels,float quotient, int stride)
        {
            for (int y = start; y < end; y++)
            {
                int currentLine = y * stride;
                for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                {
                    int oldBlue = pixels[currentLine + x];
                    int oldGreen = pixels[currentLine + x + 1];
                    int oldRed = pixels[currentLine + x + 2];

                    HSLColor c = HSLColor.FromRGB((byte)oldRed, (byte)oldGreen, (byte)oldBlue);

                    c.Luminosity *= quotient;
                    if (c.Luminosity > 1)
                        c.Luminosity = 1f;
                    Color c1 = c.ToRGB();
                    pixels[currentLine + x] = (byte)c1.B;
                    pixels[currentLine + x + 1] = (byte)c1.G;
                    pixels[currentLine + x + 2] = (byte)c1.R;
                }
            }
        }

    }
}
