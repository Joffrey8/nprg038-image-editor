﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace ImageEditorForm
{
    static class Filters
    {
        /// <summary>
        /// function takes delegates in order to edit each color chanel
        /// </summary>
        /// <param name="image"></param>
        /// <param name="Red"></param>
        /// <param name="Green"></param>
        /// <param name="Blue"></param>
        internal static void ChangeOnlyOnePixel(Bitmap image,Func<byte, byte, byte, byte> Red, 
            Func<byte, byte, byte, byte> Green, Func<byte, byte, byte, byte> Blue)
        {
            BitmapData data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, image.PixelFormat);

            int bytesPerPixel = Bitmap.GetPixelFormatSize(image.PixelFormat) / 8;

            //The stride is the width of a single row of pixels (a scan line), rounded up to a four-byte boundary
            //Number of bytes which are needed for data container
            int NumberOfBytes = data.Stride * image.Height;
            //container where we are going to save the modified pixels
            byte[] pixels = new byte[NumberOfBytes];
            //address of the first pixel in the image
            IntPtr ptrFirstPixel = data.Scan0;
            Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
            int heightInPixels = data.Height;
            int widthInBytes = data.Width * bytesPerPixel;


            for (int y = 0; y < heightInPixels; y++)
            {
                int currentLine = y * data.Stride;
                for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                {
                    byte oldBlue = pixels[currentLine + x];
                    byte oldGreen = pixels[currentLine + x + 1];
                    byte oldRed = pixels[currentLine + x + 2];

                    pixels[currentLine + x] = Blue(oldRed,oldGreen,oldBlue);
                    pixels[currentLine + x + 1] = Green(oldRed,oldGreen,oldBlue);
                    pixels[currentLine + x + 2] = Red(oldRed,oldGreen,oldBlue);
                }
            }

            Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);

            image.UnlockBits(data);
        }
    }
}
