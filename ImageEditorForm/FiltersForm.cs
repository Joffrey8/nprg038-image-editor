﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Drawing.Imaging;
using System.IO;

namespace ImageEditorForm
{
    public partial class FiltersForm : Form
    {
        public FiltersForm(Form1 f)
        {
            form = f;
            InitializeComponent();
            radioButton1.Text = "Fog";
        }
        Form1 form;
        string path;
        /// <summary>
        /// Function loads picture from file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadfile_button_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            DialogResult d = fileDialog.ShowDialog();
            if (d == DialogResult.OK)
            {
                //load the image to the graphics, show it in picturebox
                path = fileDialog.FileName;
                input_pictureBox.Image = Image.FromFile(fileDialog.FileName);
            }
        }

        /// <summary>
        /// Function loads picture from the instance of Form1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void loadCanvas_button_Click(object sender, EventArgs e)
        {
            if (form.pictureBox1.Image != null)
            {
                input_pictureBox.Image = form.pictureBox1.Image;
                path = form.path;
            }
        }

        /// <summary>
        /// function saves edited picture to file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveFile_button_Click(object sender, EventArgs e)
        {
            if (output_pictureBox.Image != null)
            {
                SaveFileDialog fileDialog = new SaveFileDialog();
                fileDialog.Filter = "Images|*.png;*.jpg;*.bmp";
                ImageFormat formatOfPicture = ImageFormat.Png; //default format
                DialogResult d = fileDialog.ShowDialog();
                if (d == DialogResult.OK)
                {
                    //if there is explicitly other format, then we would use it, otherwise .png is used
                    string ext = (Path.GetExtension(fileDialog.FileName)).ToLower();
                    if (ext == ".jpg" || ext == ".jpeg")
                        formatOfPicture = ImageFormat.Jpeg;
                    if (ext == ".bmp")
                        formatOfPicture = ImageFormat.Bmp;
                    output_pictureBox.Image.Save(fileDialog.FileName, formatOfPicture);
                }
            }
        }
        /// <summary>
        /// Function copies edited picture to the canvas of Form1
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveCanvas_button_Click(object sender, EventArgs e)
        {
            if (output_pictureBox.Image != null)
            {
                form.pictureBox1.Image = output_pictureBox.Image;
                form.path = path;
                form.imgCopy = output_pictureBox.Image;
                form.copyGraphics = Graphics.FromImage(form.imgCopy);
                form.appGraphics = form.pictureBox1.CreateGraphics();
                form.imageGraphics = Graphics.FromImage(form.pictureBox1.Image);
                form.trackBar1.Value = 10;
                form.trackBar2.Value = 0;
                form.trackBar3.Value = 0;
            }
        }

        
        private void grey_button_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(grey_button.Checked, changeIntensity, changeIntensity, changeIntensity);
        }
        
        private void sepia_button_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(sepia_button.Checked, sepiaRed, sepiaGreen, sepiaBlue);
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(radioButton1.Checked, fogRed, fogGreen, fogBlue);
        }

        private void flash_Button_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(flash_Button.Checked, flashRed, flashGreen, flashBlue);
        }

        private void frozen_button_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(frozen_button.Checked, frozenRed, frozenGreen, frozenBlue);
        }

        private void suji_button_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(suji_button.Checked, sujiRed, sujiGreen, sujiBlue);
        }

        private void dramatic_button_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(dramatic_button.Checked, dramaticRed, dramaticGreen, dramaticBlue);
        }

        private void cacao_button_CheckedChanged(object sender, EventArgs e)
        {
            executeFilter(cacao_button.Checked, cacaoRed, cacaoGreen, cacaoBlue);
        }
        /// <summary>
        /// Function takes delegates as parameters, each function takes RGB values and calculates particular color chanel
        /// </summary>
        /// <param name="b">Determines whether particular button is checked or not</param>
        /// <param name="Red"></param>
        /// <param name="Green"></param>
        /// <param name="Blue"></param>
        private void executeFilter(bool b, Func<byte, byte, byte, byte> Red, Func<byte, byte, byte, byte> Green, Func<byte, byte, byte, byte> Blue)
        {
            if (input_pictureBox.Image != null)
                if (b)
                {
                    Image output = (Image)input_pictureBox.Image.Clone();
                    Filters.ChangeOnlyOnePixel((Bitmap)output, Red, Green, Blue);
                    output_pictureBox.Image = output;
                }
        }




        


        /// <summary>
        /// Function for Grey filter
        /// </summary>
        /// <param name="Red"></param>
        /// <param name="Green"></param>
        /// <param name="Blue"></param>
        /// <returns></returns>
        private byte changeIntensity(byte Red, byte Green, byte Blue)
        {
            return (byte)((Red + Green + Blue) / 3);
        }
        
        private byte fogRed(byte Red, byte Green, byte Blue)
        {
            int result = (int)(1.3f * Red);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte fogGreen(byte Red, byte Green, byte Blue)
        {
            int result = (int)(1.7f * Green);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte fogBlue(byte Red, byte Green, byte Blue)
        {
            int result = (int)(2.3f * Blue);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }


        private byte flashRed(byte Red, byte Green, byte Blue)
        {
            int result = (int)(1.9f * Red);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte flashGreen(byte Red, byte Green, byte Blue)
        {
            int result = (int)(2.5f * Green);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte flashBlue(byte Red, byte Green, byte Blue)
        {
            int result = (int)(2.3f * Blue);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }


        private byte frozenRed(byte Red, byte Green, byte Blue)
        {
            int result = (int)(1.3f * Red);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte frozenGreen(byte Red, byte Green, byte Blue)
        {
            return Green;
        }
        private byte frozenBlue(byte Red, byte Green, byte Blue)
        {
            int result = (int)(1.5f * Blue);
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }


        private byte sepiaRed(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.393f*Red) +(0.769*Green)+(0.189f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte sepiaGreen(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.349f * Red) + (0.686 * Green) + (0.168f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte sepiaBlue(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.272f * Red) + (0.534f * Green) + (0.131f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }


        private byte sujiRed(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.393f * Red) + ((0.769f + 0.3f) * Green) + (0.189f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte sujiGreen(byte Red, byte Green, byte Blue)
        {
            int result = (int)(((0.349f+0.5f) * Red) + (0.686 * Green) + (0.168f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte sujiBlue(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.272f * Red) + (0.534f * Green) + ((0.131f +0.5f)* Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }


        private byte dramaticRed(byte Red, byte Green, byte Blue)
        {
            int result = (int)(((0.393f+0.3f) * Red) + (0.769f * Green) + (0.189f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte dramaticGreen(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.349f * Red) + ((0.686f+0.2f) * Green) + (0.168f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte dramaticBlue(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.272f * Red) + (0.534f * Green) + ((0.131f + 0.9f) * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }


        private byte cacaoRed(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.393f * Red) + (0.769f * Green) + ((0.189f+2.3f) * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte cacaoGreen(byte Red, byte Green, byte Blue)
        {
            int result = (int)((0.349f * Red) + ((0.686f + 0.5f) * Green) + (0.168f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }
        private byte cacaoBlue(byte Red, byte Green, byte Blue)
        {
            int result = (int)(((0.272f+1.3f) * Red) + (0.534f * Green) + (0.131f * Blue));
            if (result < 255)
                return (byte)result;
            else
                return 255;
        }

        
    }
}
