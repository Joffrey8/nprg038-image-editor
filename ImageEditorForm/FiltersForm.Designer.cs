﻿namespace ImageEditorForm
{
    partial class FiltersForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input_pictureBox = new System.Windows.Forms.PictureBox();
            this.output_pictureBox = new System.Windows.Forms.PictureBox();
            this.saveFile_button = new System.Windows.Forms.Button();
            this.saveCanvas_button = new System.Windows.Forms.Button();
            this.loadfile_button = new System.Windows.Forms.Button();
            this.loadCanvas_button = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sepia_button = new System.Windows.Forms.RadioButton();
            this.grey_button = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.flash_Button = new System.Windows.Forms.RadioButton();
            this.frozen_button = new System.Windows.Forms.RadioButton();
            this.suji_button = new System.Windows.Forms.RadioButton();
            this.dramatic_button = new System.Windows.Forms.RadioButton();
            this.cacao_button = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.input_pictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.output_pictureBox)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // input_pictureBox
            // 
            this.input_pictureBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.input_pictureBox.Location = new System.Drawing.Point(30, 12);
            this.input_pictureBox.Name = "input_pictureBox";
            this.input_pictureBox.Size = new System.Drawing.Size(343, 320);
            this.input_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.input_pictureBox.TabIndex = 0;
            this.input_pictureBox.TabStop = false;
            // 
            // output_pictureBox
            // 
            this.output_pictureBox.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.output_pictureBox.Location = new System.Drawing.Point(430, 12);
            this.output_pictureBox.Name = "output_pictureBox";
            this.output_pictureBox.Size = new System.Drawing.Size(343, 320);
            this.output_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.output_pictureBox.TabIndex = 1;
            this.output_pictureBox.TabStop = false;
            // 
            // saveFile_button
            // 
            this.saveFile_button.Location = new System.Drawing.Point(649, 388);
            this.saveFile_button.Name = "saveFile_button";
            this.saveFile_button.Size = new System.Drawing.Size(124, 29);
            this.saveFile_button.TabIndex = 2;
            this.saveFile_button.Text = "Save To File";
            this.saveFile_button.UseVisualStyleBackColor = true;
            this.saveFile_button.Click += new System.EventHandler(this.saveFile_button_Click);
            // 
            // saveCanvas_button
            // 
            this.saveCanvas_button.Location = new System.Drawing.Point(649, 423);
            this.saveCanvas_button.Name = "saveCanvas_button";
            this.saveCanvas_button.Size = new System.Drawing.Size(124, 29);
            this.saveCanvas_button.TabIndex = 3;
            this.saveCanvas_button.Text = "Draw To Canvas";
            this.saveCanvas_button.UseVisualStyleBackColor = true;
            this.saveCanvas_button.Click += new System.EventHandler(this.saveCanvas_button_Click);
            // 
            // loadfile_button
            // 
            this.loadfile_button.Location = new System.Drawing.Point(519, 388);
            this.loadfile_button.Name = "loadfile_button";
            this.loadfile_button.Size = new System.Drawing.Size(124, 29);
            this.loadfile_button.TabIndex = 4;
            this.loadfile_button.Text = "Load From File";
            this.loadfile_button.UseVisualStyleBackColor = true;
            this.loadfile_button.Click += new System.EventHandler(this.loadfile_button_Click);
            // 
            // loadCanvas_button
            // 
            this.loadCanvas_button.Location = new System.Drawing.Point(519, 423);
            this.loadCanvas_button.Name = "loadCanvas_button";
            this.loadCanvas_button.Size = new System.Drawing.Size(124, 29);
            this.loadCanvas_button.TabIndex = 5;
            this.loadCanvas_button.Text = "Load From Canvas";
            this.loadCanvas_button.UseVisualStyleBackColor = true;
            this.loadCanvas_button.Click += new System.EventHandler(this.loadCanvas_button_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cacao_button);
            this.groupBox1.Controls.Add(this.dramatic_button);
            this.groupBox1.Controls.Add(this.suji_button);
            this.groupBox1.Controls.Add(this.frozen_button);
            this.groupBox1.Controls.Add(this.flash_Button);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.sepia_button);
            this.groupBox1.Controls.Add(this.grey_button);
            this.groupBox1.Location = new System.Drawing.Point(30, 352);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(409, 100);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filters:";
            // 
            // sepia_button
            // 
            this.sepia_button.AutoSize = true;
            this.sepia_button.Location = new System.Drawing.Point(6, 36);
            this.sepia_button.Name = "sepia_button";
            this.sepia_button.Size = new System.Drawing.Size(52, 17);
            this.sepia_button.TabIndex = 1;
            this.sepia_button.TabStop = true;
            this.sepia_button.Text = "Sepia";
            this.sepia_button.UseVisualStyleBackColor = true;
            this.sepia_button.CheckedChanged += new System.EventHandler(this.sepia_button_CheckedChanged);
            // 
            // grey_button
            // 
            this.grey_button.AutoSize = true;
            this.grey_button.Location = new System.Drawing.Point(6, 19);
            this.grey_button.Name = "grey_button";
            this.grey_button.Size = new System.Drawing.Size(47, 17);
            this.grey_button.TabIndex = 0;
            this.grey_button.TabStop = true;
            this.grey_button.Text = "Grey";
            this.grey_button.UseVisualStyleBackColor = true;
            this.grey_button.CheckedChanged += new System.EventHandler(this.grey_button_CheckedChanged);
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(519, 458);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(254, 29);
            this.button1.TabIndex = 7;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(6, 59);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(43, 17);
            this.radioButton1.TabIndex = 2;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Fog";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // flash_Button
            // 
            this.flash_Button.AutoSize = true;
            this.flash_Button.Location = new System.Drawing.Point(59, 19);
            this.flash_Button.Name = "flash_Button";
            this.flash_Button.Size = new System.Drawing.Size(50, 17);
            this.flash_Button.TabIndex = 3;
            this.flash_Button.TabStop = true;
            this.flash_Button.Text = "Flash";
            this.flash_Button.UseVisualStyleBackColor = true;
            this.flash_Button.CheckedChanged += new System.EventHandler(this.flash_Button_CheckedChanged);
            // 
            // frozen_button
            // 
            this.frozen_button.AutoSize = true;
            this.frozen_button.Location = new System.Drawing.Point(59, 36);
            this.frozen_button.Name = "frozen_button";
            this.frozen_button.Size = new System.Drawing.Size(57, 17);
            this.frozen_button.TabIndex = 4;
            this.frozen_button.TabStop = true;
            this.frozen_button.Text = "Frozen";
            this.frozen_button.UseVisualStyleBackColor = true;
            this.frozen_button.CheckedChanged += new System.EventHandler(this.frozen_button_CheckedChanged);
            // 
            // suji_button
            // 
            this.suji_button.AutoSize = true;
            this.suji_button.Location = new System.Drawing.Point(59, 59);
            this.suji_button.Name = "suji_button";
            this.suji_button.Size = new System.Drawing.Size(42, 17);
            this.suji_button.TabIndex = 5;
            this.suji_button.TabStop = true;
            this.suji_button.Text = "Suji";
            this.suji_button.UseVisualStyleBackColor = true;
            this.suji_button.CheckedChanged += new System.EventHandler(this.suji_button_CheckedChanged);
            // 
            // dramatic_button
            // 
            this.dramatic_button.AutoSize = true;
            this.dramatic_button.Location = new System.Drawing.Point(122, 19);
            this.dramatic_button.Name = "dramatic_button";
            this.dramatic_button.Size = new System.Drawing.Size(67, 17);
            this.dramatic_button.TabIndex = 6;
            this.dramatic_button.TabStop = true;
            this.dramatic_button.Text = "Dramatic";
            this.dramatic_button.UseVisualStyleBackColor = true;
            this.dramatic_button.CheckedChanged += new System.EventHandler(this.dramatic_button_CheckedChanged);
            // 
            // cacao_button
            // 
            this.cacao_button.AutoSize = true;
            this.cacao_button.Location = new System.Drawing.Point(122, 36);
            this.cacao_button.Name = "cacao_button";
            this.cacao_button.Size = new System.Drawing.Size(56, 17);
            this.cacao_button.TabIndex = 7;
            this.cacao_button.TabStop = true;
            this.cacao_button.Text = "Cacao";
            this.cacao_button.UseVisualStyleBackColor = true;
            this.cacao_button.CheckedChanged += new System.EventHandler(this.cacao_button_CheckedChanged);
            // 
            // FiltersForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 496);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.loadCanvas_button);
            this.Controls.Add(this.loadfile_button);
            this.Controls.Add(this.saveCanvas_button);
            this.Controls.Add(this.saveFile_button);
            this.Controls.Add(this.output_pictureBox);
            this.Controls.Add(this.input_pictureBox);
            this.Name = "FiltersForm";
            this.Text = "FiltersForm";
            ((System.ComponentModel.ISupportInitialize)(this.input_pictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.output_pictureBox)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox input_pictureBox;
        private System.Windows.Forms.PictureBox output_pictureBox;
        private System.Windows.Forms.Button saveFile_button;
        private System.Windows.Forms.Button saveCanvas_button;
        private System.Windows.Forms.Button loadfile_button;
        private System.Windows.Forms.Button loadCanvas_button;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton sepia_button;
        private System.Windows.Forms.RadioButton grey_button;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton flash_Button;
        private System.Windows.Forms.RadioButton frozen_button;
        private System.Windows.Forms.RadioButton cacao_button;
        private System.Windows.Forms.RadioButton dramatic_button;
        private System.Windows.Forms.RadioButton suji_button;
    }
}