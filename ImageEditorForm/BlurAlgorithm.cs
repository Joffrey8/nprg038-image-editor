﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace ImageEditorForm
{
    static class BlurAlgorithm
    {
        /// <summary>
        /// Function blurs image, Boxing-blur Algorithm is implemented to achieve blur impression
        /// </summary>
        /// <param name="image">image to modify</param>
        /// <returns>returns modified bitmap</returns>
        public static Bitmap Blur(Bitmap image)
        {
            /*Implementation uses Marshal.Copy to access the memory
             * According to https://www.codeproject.com/Questions/867401/Why-Marshal-Copy-is-faster-than-unsafe Marshal.Copy can be even
             * faster than unsafe code 
             * */
            BitmapData data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height), ImageLockMode.ReadWrite, image.PixelFormat);

            int bytesPerPixel = Bitmap.GetPixelFormatSize(image.PixelFormat) / 8;
            
            //The stride is the width of a single row of pixels (a scan line), rounded up to a four-byte boundary
            //Number of bytes which are needed for data container
            int NumberOfBytes = data.Stride * image.Height;
            //container where we are going to save the modified pixels
            byte[] pixels = new byte[NumberOfBytes];
            //address of the first pixel in the image
            IntPtr ptrFirstPixel = data.Scan0;
            Marshal.Copy(ptrFirstPixel, pixels, 0, pixels.Length);
            int heightInPixels = data.Height;
            int widthInBytes = data.Width * bytesPerPixel;

            int firstLine = 0;
            int secondLine = data.Stride;
            int thirdLine = secondLine + data.Stride;

            //loop over the bitmap
            for (int y = 1; y < heightInPixels-1; y++)
            {
                for (int x = 0; x < widthInBytes-(2*bytesPerPixel); x = x + bytesPerPixel)
                {
                    //get the 3x3 kernel to calculate the blur for each RGB color in current pixel

                    int first1Blue = pixels[firstLine + x];
                    int second1Blue = pixels[secondLine + x];
                    int third1Blue = pixels[thirdLine + x];
                    int first2Blue = pixels[firstLine + x + bytesPerPixel];
                    int second2Blue = pixels[secondLine + x + bytesPerPixel];
                    int third2Blue = pixels[thirdLine + x + bytesPerPixel];
                    int first3Blue = pixels[firstLine + x+ (2* +bytesPerPixel)];
                    int second3Blue = pixels[secondLine + x+ (2 * +bytesPerPixel)];
                    int third3Blue = pixels[thirdLine + x+ (2 * +bytesPerPixel)];

                    int first1Green = pixels[firstLine + x + 1];
                    int second1Green = pixels[secondLine + x + 1];
                    int third1Green = pixels[thirdLine + x + 1];
                    int first2Green = pixels[firstLine + x + 1 + bytesPerPixel];
                    int second2Green = pixels[secondLine + x + 1 + bytesPerPixel];
                    int third2Green = pixels[thirdLine + x + 1 + bytesPerPixel];
                    int first3Green = pixels[firstLine + x + 1  + (2 * +bytesPerPixel)];
                    int second3Green = pixels[secondLine + x + 1 + (2 * +bytesPerPixel)];
                    int third3Green = pixels[thirdLine + x + 1 + (2 * +bytesPerPixel)];

                    int first1Red = pixels[firstLine + x + 2];
                    int second1Red = pixels[secondLine + x + 2];
                    int third1Red = pixels[thirdLine + x + 2];
                    int first2Red = pixels[firstLine + x + 2 + bytesPerPixel];
                    int second2Red = pixels[secondLine + x + 2 + bytesPerPixel];
                    int third2Red = pixels[thirdLine + x + 2 + bytesPerPixel];
                    int first3Red = pixels[firstLine + x + 2 + (2 * +bytesPerPixel)];
                    int second3Red = pixels[secondLine + x + 2 + (2 * +bytesPerPixel)];
                    int third3Red = pixels[thirdLine + x + 2 + (2 * +bytesPerPixel)];
                    

                    // calculate new pixel value - average of each kernel
                    pixels[secondLine + bytesPerPixel + x] = (byte)((first1Blue+first2Blue+first3Blue+second1Blue+second2Blue+second3Blue+third1Blue+third2Blue+third3Blue)/9);
                    pixels[secondLine + bytesPerPixel + x + 1] = (byte)((first1Green + first2Green + first3Green + second1Green + second2Green + second3Green + third1Green + third2Green + third3Green)/9);
                    pixels[secondLine + bytesPerPixel + x + 2] = (byte)((first1Red + first2Red + first3Red + second1Red + second2Red + second3Red + third1Red + third2Red + third3Red) / 9);
                }
                //move in the image to the next line
                firstLine = secondLine;
                secondLine = thirdLine;
                thirdLine += data.Stride;
            }

            
            Marshal.Copy(pixels, 0, ptrFirstPixel, pixels.Length);

            image.UnlockBits(data);

            return image;
        }
    }
}
