﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Drawing.Imaging;
using System.IO;
using System.Threading;

namespace ImageEditorForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            appGraphics = pictureBox1.CreateGraphics();
            
            pen = new Pen(Color.Black, 4);
        }

        internal Graphics imageGraphics,appGraphics; //there has to be two instances of Graphics, because otherwise changes would not be saved to image or not show up on screen
        currentAction currAction = currentAction.None;
        bool openedFile = false; //determines whether any image is loaded
        int coordinateX = -1, coordinateY = -1;
        /// <summary>
        /// true if user holds the left mouse button
        /// </summary>
        bool moving = false;
        /// <summary>
        /// true if object should be filled with color
        /// </summary>
        bool FillShape = false;
        /// <summary>
        /// Stores color and width
        /// </summary>
        Pen pen;
        internal string path;
        /// <summary>
        /// Loads image from file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoadImageItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            DialogResult d = fileDialog.ShowDialog();
            if(d == DialogResult.OK)
            {
                //load the image to the graphics, show it in picturebox
                resetValues();
                path = fileDialog.FileName;
                pictureBox1.Image = Image.FromFile(fileDialog.FileName);
                openedFile = true;
                imageGraphics = Graphics.FromImage(pictureBox1.Image);
                appGraphics = pictureBox1.CreateGraphics();
            }
            
        }
        
        /// <summary>
        /// Function saves images to file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void menuItem4_Click(object sender, EventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.Filter = "Images|*.png;*.jpg;*.bmp";
            ImageFormat formatOfPicture = ImageFormat.Png; //default format
            DialogResult d = fileDialog.ShowDialog();
            if (d == DialogResult.OK)
            {
                //if there is explicitly other format, then we would use it, otherwise .png is used
                string ext = (Path.GetExtension(fileDialog.FileName)).ToLower();
                if (ext == ".jpg" || ext == ".jpeg")
                    formatOfPicture = ImageFormat.Jpeg;
                if (ext == ".bmp")
                    formatOfPicture = ImageFormat.Bmp;
                pictureBox1.Image.Save(fileDialog.FileName, formatOfPicture);
   
            }
        }

        /// <summary>
        /// Function clears the canvas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void clear_button_Click(object sender, EventArgs e)
        {
            appGraphics.Clear(Color.White);
            imageGraphics = appGraphics;
            pictureBox1.Image = null;
            openedFile = false;
            currAction = currentAction.None;
            imageAction = ImageAction.None;
            resetValues();
        }
        
        /// <summary>
        /// Function opens a new form to choose a color and width of brush
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void brush_button_Click(object sender, EventArgs e)
        {
            using (BrushOptionsForm bf = new BrushOptionsForm())
            {
                if(bf.ShowDialog() == DialogResult.OK)
                {
                    currAction = currentAction.Brush;
                    pen = new Pen(bf.BrushColor, bf.WidthOfPen);
                }
            }
        }

        /// <summary>
        /// Function stores starting location for shapes/text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (currAction != currentAction.None)
            {
                moving = true;
                coordinateX = e.X;
                coordinateY = e.Y;
            }
        }
        /// <summary>
        /// Function draws objects to canvas (shapes,text,..),uses stored location (saved by MouseDown function) to determine the region where the object should be placed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            //draws corresponding shape, which is saved in var currAction (currentAction)
            switch (currAction)
            {
                case currentAction.Brush:
                    break;
                case currentAction.LineShape:
                    appGraphics.DrawLine(pen, new Point(coordinateX, coordinateY), e.Location);
                    if (openedFile) //if there is image on the canvas aswell, the the graphics has to be drawn there
                        imageGraphics.DrawLine(pen, new Point(coordinateX, coordinateY), e.Location);
                    if(imgCopy!=null)
                        copyGraphics.DrawLine(pen, new Point(coordinateX, coordinateY), e.Location);
                    break;
                case currentAction.RectangleShape:
                    if (FillShape)
                    {
                        Brush b = new SolidBrush(pen.Color);
                        appGraphics.FillRectangle(b, Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));
                        if (openedFile) 
                            imageGraphics.FillRectangle(b, Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));
                        if (imgCopy != null)
                            copyGraphics.FillRectangle(b, Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));

                    }
                    else
                    {
                        appGraphics.DrawRectangle(pen, Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));
                        if (openedFile)
                            imageGraphics.DrawRectangle(pen, Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));
                        if (imgCopy != null)
                            copyGraphics.DrawRectangle(pen, Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));

                    }
                    break;
                case currentAction.ElipseShape:
                    Rectangle r = new Rectangle(Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));
                    if (FillShape) //true if the shape should be filled with color
                    {
                        Brush b = new SolidBrush(pen.Color);
                        appGraphics.FillEllipse(b, r);
                        if (openedFile)
                            imageGraphics.FillEllipse(b, r);
                        if (imgCopy != null)
                            copyGraphics.FillEllipse(b, r);
                    }
                    else
                    {
                        appGraphics.DrawEllipse(pen, r);
                        if (openedFile)
                            imageGraphics.DrawEllipse(pen, r);
                        if (imgCopy != null)
                            copyGraphics.DrawEllipse(pen, r);
                    }
                    break;
                case currentAction.WriteText:
                    //rectangle determines the region of text, uses the saved mouse-down location + current mouse location 
                    RectangleF rf = new RectangleF(Math.Min(coordinateX, e.X), Math.Min(coordinateY, e.Y), Math.Abs(coordinateX - e.X), Math.Abs(coordinateY - e.Y));
                    appGraphics.DrawString(inputText, new Font(fontStyle, pen.Width), new SolidBrush(pen.Color), rf);
                    if(openedFile)
                        imageGraphics.DrawString(inputText, new Font(fontStyle, pen.Width), new SolidBrush(pen.Color), rf);
                    if (imgCopy != null)
                        copyGraphics.DrawString(inputText, new Font(fontStyle, pen.Width), new SolidBrush(pen.Color), rf);
                    break;
                default:
                    break;
            }
            moving = false;
            coordinateX = -1;
            coordinateY = -1;
        }
        /// <summary>
        /// button opens a form, where user can choose between various shapes to draw (rectangle, line,..)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void shapes_button_Click(object sender, EventArgs e)
        {
            using (ShapesForm sf = new ShapesForm())
            {
                if(sf.ShowDialog() == DialogResult.OK)
                {
                    // variable WhichSpape depends on choice of shape in ShapesForm
                    switch(sf.WhichShape)
                    {
                        case 1:
                            currAction = currentAction.LineShape;
                            break;
                        case 2:
                            currAction = currentAction.RectangleShape;
                            break;
                        case 3:
                            currAction = currentAction.ElipseShape;
                            break;
                        default:
                            //cannot actually happen
                            break;
                    }
                    //true if the check-button in ShapesForm is set
                    FillShape = sf.FillTheShape;
                    pen = new Pen(sf.ShapeColor, sf.WidthOfPen);
                }
            }
        }

        //these variables hold information about font Style and text input, if currAction == currentAction.WriteText (if we want to write text to canvas), otherwise are not important
        string fontStyle;
        string inputText;
        /// <summary>
        /// button opens a form, where user can set option for text -input text, font, fontsize, fontcolor
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void text_button_Click(object sender, EventArgs e)
        {
            using (TextForm tf = new TextForm())
            {
                if(tf.ShowDialog() == DialogResult.OK)
                {
                    pen = new Pen(tf.FontColor, tf.FontSize);
                    fontStyle = tf.fontStyle;
                    inputText = tf.InputText;
                    currAction = currentAction.WriteText;
                }
            }
        }

        

        private void reset_button_Click(object sender, EventArgs e)
        {
            appGraphics.Clear(Color.White);
            resetValues();
            Image image = Image.FromFile(path);
            pictureBox1.Image = image;
            openedFile = true;
            imageGraphics = Graphics.FromImage(pictureBox1.Image);
            currAction = currentAction.None;
            imageAction = ImageAction.None;
        }


        /// <summary>
        /// Executes a blur algorithm at image in pictureBox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void blur_button_Click(object sender, EventArgs e)
        {
            if (algThread == null || !algThread.IsAlive)
            {
                if (openedFile)
                {
                    algThread = new Thread(() =>
                    {
                        Image tmp = pictureBox1.Image;
                        tmp = BlurAlgorithm.Blur((Bitmap)tmp);
                        this.Invoke(new Action(()=> { pictureBox1.Image = tmp; }));
                    });
                    algThread.Start();
                }
            }
            if (imageAction != ImageAction.Blur)
            {
                imgCopy = pictureBox1.Image;
                copyGraphics = Graphics.FromImage(imgCopy);
            }
            imageAction = ImageAction.Blur;
            currAction = currentAction.None;
            trackBar1.Value = 10;
        }
        internal Image imgCopy;
        internal Graphics copyGraphics;

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            if (algThread == null || !algThread.IsAlive)
            {
                if (imageAction != ImageAction.Brightness)
                {
                    imgCopy = pictureBox1.Image;
                    copyGraphics = Graphics.FromImage(imgCopy);
                }
                if (openedFile)
                {
                    if (imgCopy == null)
                    {
                        imgCopy = (Image)pictureBox1.Image.Clone();
                        copyGraphics = Graphics.FromImage(imgCopy);
                    }
                    float quotient = trackBar1.Value / 10f;

                    Image tmp2 = (Bitmap)imgCopy.Clone();
                    algThread = new Thread(() =>
                    {
                        //Invoke(new Action(() => { pictureBox1.Image = (Image)imgCopy.Clone(); }));
                        Image tmp = tmp2;
                        tmp = ModifyBrightnessAlgorithm.ChangeBrightness((Bitmap)tmp, parallel_checkBox.Checked, quotient);
                        pictureBox1.Invoke(new Action(() =>
                        {
                            pictureBox1.Image = tmp;
                            imageGraphics = Graphics.FromImage(pictureBox1.Image);
                            
                        }));
                    });
                    algThread.Start();
                    imageAction = ImageAction.Brightness;
                    currAction = currentAction.None;
                }
            }
        }

        /// <summary>
        /// if currAction is set to brush mode, then function draws on canvas when mouse is moving
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if(moving && coordinateX!=-1 && coordinateY!=-1 &&currAction == currentAction.Brush)
            {
                appGraphics.DrawLine(pen, new Point(coordinateX, coordinateY), e.Location);
                if(openedFile)
                    imageGraphics.DrawLine(pen, new Point(coordinateX, coordinateY), e.Location);
                if(imgCopy!=null)
                    copyGraphics.DrawLine(pen, new Point(coordinateX, coordinateY), e.Location);
                coordinateX = e.X;
                coordinateY = e.Y;
            }
        }


        //if any value is changed,then ChangeSize() function is executed
        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            ChangeSize();
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            ChangeSize();
        }

        /// <summary>
        /// defines current action - whether user wants to draw shapes/text/...
        /// </summary>
        enum currentAction { None,Brush,LineShape,RectangleShape,ElipseShape,WriteText };
        enum ImageAction { None,Blur, Size, Brightness};
        ImageAction imageAction=ImageAction.None;

        /// <summary>
        /// Shows new form, where current image (or image from file) can be edited by various filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void filters_button_Click(object sender, EventArgs e)
        {
            FiltersForm f = new FiltersForm(this);
            if(f.ShowDialog() == DialogResult.OK)
            {
                imageAction = ImageAction.None;
                currAction = currentAction.None;
            }
            
        }

        private void resetValues()
        {
            trackBar1.Value = 10;
            trackBar2.Value = 0;
            trackBar3.Value = 0;

            imgCopy = null;
        }

        /// <summary>
        /// Function is executed if trackbar values are changed. Function changes size of the picture
        /// </summary>
        private void ChangeSize()
        {
            if (openedFile)
            {
                imageAction = ImageAction.Size;
                currAction = currentAction.None;
                if (imgCopy == null)
                {
                    imgCopy = (Image)pictureBox1.Image.Clone();
                    copyGraphics = Graphics.FromImage(imgCopy);
                }
                Size changedSize = new Size((int)(imgCopy.Width + ((imgCopy.Width / 20f) * trackBar2.Value)), 
                    (int)(imgCopy.Height + ((imgCopy.Height / 20f) * trackBar3.Value)));
                Image i = new Bitmap(changedSize.Width, changedSize.Height);
                Graphics g = Graphics.FromImage((Bitmap)i);
                g.DrawImage(pictureBox1.Image, new Rectangle(Point.Empty, changedSize));
                pictureBox1.Image = i;
            }
        }
        Thread algThread;
    }
}