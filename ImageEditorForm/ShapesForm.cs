﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageEditorForm
{
    public partial class ShapesForm : Form
    {
        public Color ShapeColor { get; set; } = Color.Black;
        /// <summary>
        /// 1 is Line, 2 is Rectangle, 3 is Elipse
        /// </summary>
        public byte WhichShape { get; set; } = 1;
        public int WidthOfPen { get; set; } = 4;
        public bool FillTheShape { get; set; } = false;
        public ShapesForm()
        {
            
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ColorDialog c1 = new ColorDialog();
            c1.ShowDialog();
            ShapeColor = c1.Color;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            WhichShape = 1;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            WhichShape = 2;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            WhichShape = 3;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(comboBox1.Text, out int result))
            {
                WidthOfPen = result;
            }
            else
                MessageBox.Show("Width has to be an integer!", "Wrong Input", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void ShapesForm_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = null;
            comboBox1.SelectedText = "--select width--";
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(comboBox1.Text, out int result))
            {
                WidthOfPen = result;
            }
            
        }

        private void FillTheShapeBox_CheckedChanged(object sender, EventArgs e)
        {
            if (FillTheShapeBox.Checked == true)
                FillTheShape = true;
            else
                FillTheShape = false;

        }
    }
}
