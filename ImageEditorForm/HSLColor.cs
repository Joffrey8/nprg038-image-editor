﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("FunctionTests")]


namespace ImageEditorForm
{
    class HSLColor
    {
        
        public float Hue;
        public float Saturation;
        public float Luminosity;

        public HSLColor(float H, float S, float L)
        {
            Hue = H;
            Saturation = S;
            Luminosity = L;
        }

        /// <summary>
        /// Given a Color instance function converts RGB color to HSL color
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        internal static HSLColor FromRGB(Color c)
        {
            return FromRGB(c.R, c.G, c.B);
        }
        /// <summary>
        /// Given R,G,B values function calculates a HSL color
        /// </summary>
        /// <param name="r"></param>
        /// <param name="g"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        internal static HSLColor FromRGB(byte r, byte g, byte b)
        {
            float r1 = r / 255.0f;
            float g1 = g / 255.0f;
            float b1 = b / 255.0f;

            float tmp1 = Math.Min(r1, g1);
            float Minimum = Math.Min(b1, tmp1);

            tmp1 = Math.Max(r1, g1);
            float Maximum = Math.Max(b1, tmp1);

            float L = (Minimum + Maximum) / 2;
            float S, H;

            if (Minimum == Maximum)
            {
                S = 0;
                H = 0;
            }
            else
            {
                if (L > 0.5)
                {
                    S = (Maximum - Minimum) / (2.0f - Maximum - Minimum);
                }
                else
                {
                    S = (Maximum - Minimum) / (Maximum + Minimum);
                }
                if (r1 == Maximum)
                    H = (g1 - b1) / (Maximum - Minimum);
                else if (g1 == Maximum)
                    H = 2.0f + ((b1 - r1) / (Maximum - Minimum));
                else
                    H = 4.0f + ((r1 - g1) / (Maximum - Minimum));
                H = H * 60f;
                if (H < 0) H += 360;
            }
            return new HSLColor(H, S, L);

        }
        /// <summary>
        /// Function calculates RGB color from this instance of HSL color
        /// </summary>
        /// <returns></returns>
        internal Color ToRGB()
        {
            if (Hue == 0 || Saturation == 0)
                return Color.FromArgb((byte)(Luminosity * 255), (byte)(Luminosity * 255), (byte)(Luminosity * 255));
            else
            {
                float tmp1;
                if (Luminosity < 0.5f)
                    tmp1 = Luminosity * (1.0f + Saturation);
                else
                    tmp1 = Luminosity + Saturation - (Luminosity * Saturation);
                float tmp2 = (2 * Luminosity) - tmp1;
                Hue /= 360;
                float tmp_R = Hue + 0.333f;
                if (tmp_R > 1)
                    tmp_R -= 1;
                float tmp_G = Hue;
                float tmp_B = Hue - 0.333f;
                if (tmp_B < 0)
                    tmp_B += 1;
                float Red = computeColorChannel(tmp1,tmp2,tmp_R), Green= computeColorChannel(tmp1, tmp2, tmp_G), Blue= computeColorChannel(tmp1, tmp2, tmp_B);
                return Color.FromArgb((byte)(Red * 255), (byte)(Green * 255), (byte)(Blue * 255));

            }
        }
        /// <summary>
        /// Three tests need be to runed to determine which formula should be used to calculate particular color chanel for each color chanel (R,G,B)
        /// </summary>
        /// <param name="tmp1"></param>
        /// <param name="tmp2"></param>
        /// <param name="tmpChanel">tmp_R,tmp_G or tmp_B</param>
        /// <returns></returns>
        private float computeColorChannel(float tmp1, float tmp2, float tmpChanel)
        {
            float result;
            if(6f * tmpChanel < 1f)
                    result = (tmp2 + ((tmp1 - tmp2) * 6f * tmpChanel));
            else if (2f * tmpChanel < 1f)
                result = (tmp1);
            else if (3f * tmpChanel < 2f)
                result = (tmp2 + ((tmp1 - tmp2) * (0.666f - tmpChanel) * 6f));
            else
                result = tmp2;
            return result;
        }
    }
}
