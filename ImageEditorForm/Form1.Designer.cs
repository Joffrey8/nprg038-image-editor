﻿namespace ImageEditorForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.clear_button = new System.Windows.Forms.Button();
            this.brush_button = new System.Windows.Forms.Button();
            this.shapes_button = new System.Windows.Forms.Button();
            this.text_button = new System.Windows.Forms.Button();
            this.reset_button = new System.Windows.Forms.Button();
            this.blur_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.parallel_checkBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.filters_button = new System.Windows.Forms.Button();
            this.LoadImageItem = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.mainMenu1 = new System.Windows.Forms.MainMenu(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pictureBox1.Location = new System.Drawing.Point(4, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(566, 369);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // clear_button
            // 
            this.clear_button.Location = new System.Drawing.Point(856, 624);
            this.clear_button.Name = "clear_button";
            this.clear_button.Size = new System.Drawing.Size(75, 23);
            this.clear_button.TabIndex = 1;
            this.clear_button.Text = "Clear";
            this.clear_button.UseVisualStyleBackColor = true;
            this.clear_button.Click += new System.EventHandler(this.clear_button_Click);
            // 
            // brush_button
            // 
            this.brush_button.Location = new System.Drawing.Point(856, 0);
            this.brush_button.Name = "brush_button";
            this.brush_button.Size = new System.Drawing.Size(75, 23);
            this.brush_button.TabIndex = 2;
            this.brush_button.Text = "Brush";
            this.brush_button.UseVisualStyleBackColor = true;
            this.brush_button.Click += new System.EventHandler(this.brush_button_Click);
            // 
            // shapes_button
            // 
            this.shapes_button.Location = new System.Drawing.Point(856, 29);
            this.shapes_button.Name = "shapes_button";
            this.shapes_button.Size = new System.Drawing.Size(75, 23);
            this.shapes_button.TabIndex = 3;
            this.shapes_button.Text = "Shapes";
            this.shapes_button.UseVisualStyleBackColor = true;
            this.shapes_button.Click += new System.EventHandler(this.shapes_button_Click);
            // 
            // text_button
            // 
            this.text_button.Location = new System.Drawing.Point(856, 58);
            this.text_button.Name = "text_button";
            this.text_button.Size = new System.Drawing.Size(75, 23);
            this.text_button.TabIndex = 4;
            this.text_button.Text = "Text";
            this.text_button.UseVisualStyleBackColor = true;
            this.text_button.Click += new System.EventHandler(this.text_button_Click);
            // 
            // reset_button
            // 
            this.reset_button.Location = new System.Drawing.Point(856, 578);
            this.reset_button.Name = "reset_button";
            this.reset_button.Size = new System.Drawing.Size(75, 40);
            this.reset_button.TabIndex = 6;
            this.reset_button.Text = "Reset Photo";
            this.reset_button.UseVisualStyleBackColor = true;
            this.reset_button.Click += new System.EventHandler(this.reset_button_Click);
            // 
            // blur_button
            // 
            this.blur_button.Location = new System.Drawing.Point(856, 87);
            this.blur_button.Name = "blur_button";
            this.blur_button.Size = new System.Drawing.Size(75, 23);
            this.blur_button.TabIndex = 7;
            this.blur_button.Text = "Blur";
            this.blur_button.UseVisualStyleBackColor = true;
            this.blur_button.Click += new System.EventHandler(this.blur_button_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(835, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Change Brightness";
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(827, 141);
            this.trackBar1.Maximum = 15;
            this.trackBar1.Minimum = 5;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(104, 45);
            this.trackBar1.TabIndex = 10;
            this.trackBar1.Value = 10;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // parallel_checkBox
            // 
            this.parallel_checkBox.AutoSize = true;
            this.parallel_checkBox.Location = new System.Drawing.Point(815, 169);
            this.parallel_checkBox.Name = "parallel_checkBox";
            this.parallel_checkBox.Size = new System.Drawing.Size(116, 17);
            this.parallel_checkBox.TabIndex = 11;
            this.parallel_checkBox.Text = "Parallel Execution?";
            this.parallel_checkBox.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(862, 221);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Resize";
            // 
            // trackBar2
            // 
            this.trackBar2.Location = new System.Drawing.Point(827, 288);
            this.trackBar2.Minimum = -10;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(104, 45);
            this.trackBar2.TabIndex = 13;
            this.trackBar2.TickFrequency = 2;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // trackBar3
            // 
            this.trackBar3.Location = new System.Drawing.Point(827, 237);
            this.trackBar3.Minimum = -10;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(104, 45);
            this.trackBar3.TabIndex = 14;
            this.trackBar3.TickFrequency = 2;
            this.trackBar3.Scroll += new System.EventHandler(this.trackBar3_Scroll);
            // 
            // filters_button
            // 
            this.filters_button.Location = new System.Drawing.Point(856, 346);
            this.filters_button.Name = "filters_button";
            this.filters_button.Size = new System.Drawing.Size(75, 23);
            this.filters_button.TabIndex = 15;
            this.filters_button.Text = "Filters";
            this.filters_button.UseVisualStyleBackColor = true;
            this.filters_button.Click += new System.EventHandler(this.filters_button_Click);
            // 
            // LoadImageItem
            // 
            this.LoadImageItem.Checked = true;
            this.LoadImageItem.Index = 0;
            this.LoadImageItem.Text = "Load Image..";
            this.LoadImageItem.Click += new System.EventHandler(this.LoadImageItem_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Checked = true;
            this.menuItem4.Index = 1;
            this.menuItem4.Text = "Save Image As..";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.LoadImageItem,
            this.menuItem4});
            this.menuItem1.Text = "File";
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.menuItem1});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(943, 659);
            this.Controls.Add(this.filters_button);
            this.Controls.Add(this.trackBar3);
            this.Controls.Add(this.trackBar2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.parallel_checkBox);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.blur_button);
            this.Controls.Add(this.reset_button);
            this.Controls.Add(this.text_button);
            this.Controls.Add(this.shapes_button);
            this.Controls.Add(this.brush_button);
            this.Controls.Add(this.clear_button);
            this.Controls.Add(this.pictureBox1);
            this.Menu = this.mainMenu1;
            this.Name = "Form1";
            this.Text = "Image Editor";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button clear_button;
        private System.Windows.Forms.Button brush_button;
        private System.Windows.Forms.Button shapes_button;
        private System.Windows.Forms.Button text_button;
        private System.Windows.Forms.Button reset_button;
        private System.Windows.Forms.Button blur_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox parallel_checkBox;
        private System.Windows.Forms.Label label2;
        internal System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button filters_button;
        private System.Windows.Forms.MenuItem LoadImageItem;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MainMenu mainMenu1;
        public System.Windows.Forms.TrackBar trackBar1;
        public System.Windows.Forms.TrackBar trackBar2;
        public System.Windows.Forms.TrackBar trackBar3;
    }
}

