﻿namespace ImageEditorForm
{
    partial class BrushOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKbutton = new System.Windows.Forms.Button();
            this.cancel_button = new System.Windows.Forms.Button();
            this.color_button = new System.Windows.Forms.Button();
            this.widthBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // OKbutton
            // 
            this.OKbutton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKbutton.Location = new System.Drawing.Point(159, 136);
            this.OKbutton.Name = "OKbutton";
            this.OKbutton.Size = new System.Drawing.Size(75, 23);
            this.OKbutton.TabIndex = 0;
            this.OKbutton.Text = "OK";
            this.OKbutton.UseVisualStyleBackColor = true;
            // 
            // cancel_button
            // 
            this.cancel_button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancel_button.Location = new System.Drawing.Point(240, 136);
            this.cancel_button.Name = "cancel_button";
            this.cancel_button.Size = new System.Drawing.Size(75, 23);
            this.cancel_button.TabIndex = 1;
            this.cancel_button.Text = "Cancel";
            this.cancel_button.UseVisualStyleBackColor = true;
            // 
            // color_button
            // 
            this.color_button.Location = new System.Drawing.Point(12, 12);
            this.color_button.Name = "color_button";
            this.color_button.Size = new System.Drawing.Size(156, 40);
            this.color_button.TabIndex = 2;
            this.color_button.Text = "Brush Color";
            this.color_button.UseVisualStyleBackColor = true;
            this.color_button.Click += new System.EventHandler(this.color_button_Click);
            // 
            // widthBox
            // 
            this.widthBox.FormattingEnabled = true;
            this.widthBox.Items.AddRange(new object[] {
            "1",
            "4",
            "8",
            "12",
            "16",
            "32",
            "64"});
            this.widthBox.Location = new System.Drawing.Point(12, 58);
            this.widthBox.Name = "widthBox";
            this.widthBox.Size = new System.Drawing.Size(121, 21);
            this.widthBox.TabIndex = 3;
            this.widthBox.SelectedIndexChanged += new System.EventHandler(this.widthBox_SelectedIndexChanged);
            // 
            // BrushOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 171);
            this.Controls.Add(this.widthBox);
            this.Controls.Add(this.color_button);
            this.Controls.Add(this.cancel_button);
            this.Controls.Add(this.OKbutton);
            this.Name = "BrushOptionsForm";
            this.Text = "BrushOptionsForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OKbutton;
        private System.Windows.Forms.Button cancel_button;
        private System.Windows.Forms.Button color_button;
        private System.Windows.Forms.ComboBox widthBox;
    }
}