﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;

namespace ImageEditorForm
{
    public partial class TextForm : Form
    {
        public TextForm()
        {
            InitializeComponent();
        }
        public Color FontColor { get; set; } = Color.Black;
        public int FontSize { get; set; } = 4;
        public string fontStyle { get; set; }
        public string InputText { get; set; }
        private void TextForm_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedItem = null;
            comboBox1.SelectedText = "--select Text Size--";
            fontBox.SelectedItem = null;
            textBox1.SelectedText = "--Enter Input Text--";
            
            FontFamily[] fontFamilies;

            //gets all the instaled font Collection and prints it into the comboBox
            InstalledFontCollection installedFontCollection = new InstalledFontCollection();

            // Get the array of FontFamily objects.
            fontFamilies = installedFontCollection.Families;
            //sets the first font as default - to avoid null exceptions
            fontStyle = fontFamilies[0].Name;
            //adds all instaled fonts into comboBox
            fontBox.Items.AddRange(fontFamilies.Select(x => x.Name).ToArray());            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(comboBox1.Text, out int result))
            {
                FontSize = result;
            }
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            if (Int32.TryParse(comboBox1.Text, out int result))
            {
                FontSize = result;
            }
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            InputText = textBox1.Text;
        }

        private void fontBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            fontStyle = fontBox.Text;
        }

        private void color_button_Click(object sender, EventArgs e)
        {
            ColorDialog c1 = new ColorDialog();
            c1.ShowDialog();
            FontColor = c1.Color;
        }
    }
}
